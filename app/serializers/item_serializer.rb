class ItemSerializer < ActiveModel::Serializer
  attributes :id,
             :name,
             :tag_list,
             :completion_date,
             :completed,
             :pending,
             :note,
             :spent_time
end
