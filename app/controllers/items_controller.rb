class ItemsController < ApplicationController
  before_filter :authenticate_request!
  require 'csv'

  def index   
    items = @current_user.items
    retrun_item items
  end

  def create    
    item = @current_user.items.new( item_params )
    if item.save
      retrun_item item
    else
      return_error item
    end
  end

  def pending
    items =  @current_user.items.pending    
    retrun_item items
  end

  def completed
    items =  @current_user.items.completed    
    retrun_item items
  end

  def update
    item =  @current_user.items.find_by_id( params[:id] )        
    item.update_attributes( item_params )
    retrun_item item
  end

  def generate_report
    attributes = %w{id name tag_list spent_time}

    data = CSV.generate do |csv|
      csv << attributes
      grouped_item_by_tag.each do |group|      
        csv <<  attributes.map{ |attr|
          case attr
          when "spent_time"
            group[1].collect(&attr.parameterize.underscore.to_sym).map(&:to_f).sum
          when "tag_list"  
            group[0]
          else
            group[1].collect(&attr.parameterize.underscore.to_sym)
          end  
        }        
      end
      get_item_without_tags.each do |item|         
        csv << item.attributes.values_at(*attributes)
      end
    end      
    render json: { data: data}    
  end

  private
    def item_params
      params[:item][:completion_date]=Date.strptime(params[:item][:completion_date], "%m/%d/%Y") if params[:item][:completion_date].present?
      params.require(:item).permit(:name, :tag_list, :completion_date, :spent_time, :note, :pending, :completed)
    end

    def retrun_item(item)
      render json: item,
           status: :ok
    end

    def return_error(item)
      render json: { errors: item.errors },
           status: :bad_request
    end

    def grouped_item_by_tag
      grouped = {}
      @current_user.items.each do |item|
        if item.tag_list.present?
          item.tag_list.each do |tag|
            grouped[tag] ||= []
            grouped[tag] << item
          end       
        end  
      end
      return grouped
    end

    def get_item_without_tags
      @current_user.items.collect{|x| x if x.tag_list.blank?}.flatten.compact if @current_user.items.present?
    end
end
