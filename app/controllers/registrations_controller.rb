class RegistrationsController < Devise::RegistrationsController
  respond_to :json
  
  def create
    user = User.create(sign_up_params)

    if user.valid?
      retrun_auth_token user
    else
      return_error user
    end
  end

  private

  def sign_up_params
    params
      .require(:user)
      .permit(
        :name,
        :email,
        :password,
        :password_confirmation
      )
  end

  def retrun_auth_token(user)
    render json: { 
          user: user,
          token: AuthToken.encode(user.token_payload) 
          },
          status: :ok
  end

  def return_error(user)
    render json: { errors: user.errors },
           status: :bad_request
  end

end
