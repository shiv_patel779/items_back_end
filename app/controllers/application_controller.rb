class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protected

  def authenticate_request!
    @current_user = authenticate_user_from_jwt    
    return false unless @current_user
  end


  private

  def authenticate_user_from_jwt
    begin
      _, token = request.headers['Authorization'].split
      decoded_token = AuthToken.decode(token)
      User.find(decoded_token[:user_id])
    rescue Exception => e
      unauthorized
      return false
    end    
  end

  def unauthorized
    render json: { error: { message: 'Unauthorized' } },
           status: :unauthorized
  end
end
