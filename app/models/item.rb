class Item < ApplicationRecord
  acts_as_taggable # Alias for acts_as_taggable_on :tags
  acts_as_taggable_on :lag_lists

  belongs_to :user

  scope :pending, -> { where(pending: true, completed: false) }
  scope :completed, -> { where(completed: true) }
  validates :spent_time , numericality: { only_integer: true }, if: 'spent_time.present?'
  validate :completion_date_cannot_be_in_the_past

  def completion_date_cannot_be_in_the_past
    if completion_date.present? && completion_date < Date.today
      errors.add(:completion_date, "can't be in the past")
    end
  end

  def self.check_completion_date
    puts "hiii"
    # if list ( that not completed yet ) are automatically moved (dated) the next day of current date.
    Item.where("completed=? AND completion_date < ?", false, Date.today).update_all(completion_date: Date.today+1)
  end
end
