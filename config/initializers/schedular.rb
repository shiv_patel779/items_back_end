require 'rufus-scheduler'

scheduler = Rufus::Scheduler.new


scheduler.cron '0 00 * * *' do
  # do testing every day midnight
  Item.check_completion_date
end


