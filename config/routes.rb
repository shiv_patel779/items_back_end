Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
	devise_for :users,
             defaults: { format: :json },
             skip: [:registrations, :sessions]
  as :user do
    post '/registrations' => 'registrations#create', as: :user_registration
    post '/login' => 'sessions#create', as: :user_session
  end

  resources :items, only: [:index, :create, :update] do
    collection do
      get 'pending'
      get 'completed'
      get 'generate_report'
    end
  end
end
