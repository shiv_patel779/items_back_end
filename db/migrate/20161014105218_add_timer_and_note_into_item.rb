class AddTimerAndNoteIntoItem < ActiveRecord::Migration[5.0]
  def change
    add_column :items, :note, :text
    add_column :items, :spent_time, :string, default: "0"
  end
end
