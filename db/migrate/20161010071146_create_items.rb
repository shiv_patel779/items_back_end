class CreateItems < ActiveRecord::Migration[5.0]
  def change
    create_table :items do |t|
      t.string :name
      t.integer :user_id
      t.date :completion_date
      t.string :tag_list
      t.boolean :pending, default: false
      t.boolean :completed, default: false

      t.timestamps
    end
  end
end
